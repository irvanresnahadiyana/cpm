<!DOCTYPE html>
 <?php

      $baseUrl = Yii::app()->theme->baseUrl; 

      $cs = Yii::app()->getClientScript();

      Yii::app()->clientScript->registerCoreScript('jquery');

    ?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PT. Citra Pamungkas Mandiri</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $baseUrl;?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl;?>/css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="<?php echo $baseUrl;?>/images/logo.jpg" alt="logo" width="100%">
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Product & Services</a>
                    </li>
                    <li>
                        <a href="#">About Us</a>
                    </li>
                    <li>
                        <a href="#">Experience</a>
                    </li>
                    <li>
                        <a href="#">News & Articles</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Half Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators carousel-indicators-index" style="margin-top:-50px !important;">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('<?php echo $baseUrl;?>/images/slide-1.jpg');"></div>
            </div>
         <div class="item">
                <div class="fill" style="background-image:url('<?php echo $baseUrl;?>/images/slide-2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $baseUrl;?>/images/slide-3.jpg');"></div>
            </div>
        </div>

    </header>

    <!-- content -->
    <div class="container">
        <div class="content">
            <div class="col-md-4">
              <h2>Product & Services</h2>
            </div>
            <div class="col-md-8">
                <h2>Unspecialized in the environmental sector, we provide solutions towards business sustainability </h2>
                <p>As a company expertise in the field of environment, IEC has been set as your partner to build sustainable enterprises which have strong commitment to environmental conservation and social responsibility. As a company expertise in the field of environment, IEC has been set as your partner to build sustainable enterprises which have strong commitment to environmental conservation and social responsibility.</p>
                <p>As a company expertise in the field of environment, IEC has been set as your partner to build sustainable enterprises which have strong commitment to environmental conservation and social responsibility. As a company expertise in the field of environment, IEC has been set as your partner to build sustainable enterprises which have strong commitment to environmental conservation and social responsibility.</p>
            </div>
        </div>  
    </div>
    

    <!-- Page Content -->
    <footer class="footer-home">
        <div class="container">    
            <div class="row">
                <div class="col-md-4">
                    <p>HAK CIPTA ©2015 CPM</p>
                </div>
                <div class="col-md-8 menu-footer">
                    <a href="#">KARIR</a>
                    <a href="#">STUDI KASUS</a>
                    <a href="#">PETA SITUS</a>
                    <a href="#">KEBIJAKAN PRIVASI</a>
                    <a href="#">PERATURAN PENGGUNAAN</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- /.container -->
    
    <!-- jQuery -->
    <script src="<?php echo $baseUrl;?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $baseUrl;?>/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

    <!-- circle menu -->
    <script src="<?php echo $baseUrl;?>/js/socialCircle.js"></script> 
    <script type="text/javascript">
    $( ".socialCircle-center" ).socialCircle({
        rotate: 0,
        radius:200,
        circleSize: 2,
        speed:500
    });
    </script>
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-36251023-1']);
      _gaq.push(['_setDomainName', 'jqueryscript.net']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>

</body>

</html>
