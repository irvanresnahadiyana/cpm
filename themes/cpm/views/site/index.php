<!DOCTYPE html>
  <?php

      $baseUrl = Yii::app()->theme->baseUrl; 

      $cs = Yii::app()->getClientScript();

      Yii::app()->clientScript->registerCoreScript('jquery');

    ?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PT. Citra Pamungkas Mandiri</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $baseUrl;?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl;?>/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="index">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <div class="socmed-icon">
                    <a href="#"><img src="<?php echo $baseUrl;?>/images/fb.png" alt="Facebook"></a>
                    <a href="#"><img src="<?php echo $baseUrl;?>/images/twitter.png" alt="Twitter"></a>
                </div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="navbar" class="navbar-collapse collapse">
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <!-- <a href="#"><img src="images/ina.png" alt="Ina"></a>
                        <a href="#"><img src="images/eng.png" alt="Eng"></a> -->
                        &nbsp;
                        <input class="form-search" type="text" placeholder="SEARCH">
                    </div>
                </form>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- logo view -->
    <div class="container">
        <div class="logo-view row">  
            <div class="col-md-6">
                <br>
                <h1>PT. CPM</h1>
            </div>
            <div class="logo-img col-md-6">
                <img src="<?php echo $baseUrl;?>/images/CPM.png" alt="logo" style="width:40%;padding-top:6px;right:0;">
            </div>        
        </div>  
    </div>

    <!-- Half Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide carousel-index">
        <div class="socialCircle-container">
          <div class="socialCircle-item"><a href="1">About Us</a></div>
          <div class="socialCircle-item"><a href="<?php echo Yii::app()->controller->createUrl('/site/product');?>">Product</a></div>
          <div class="socialCircle-item"><a href="1">Menu 3</a></div>
          <div class="socialCircle-item"><a href="1">Menu 4</a></div>
          <div class="socialCircle-item"><a href="1">Menu 5</a></div>
          <div class="socialCircle-item"><a href="1">Menu 6</a></div>
          <div class="socialCircle-center closed">Menu</div>
        </div>
        <!-- Indicators -->
        <!-- <div class="container">
        <ol class="carousel-indicators carousel-indicators-index">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        </div> -->
        <!-- Wrapper for Slides -->
        
        <div class="carousel-inner carousel-inner-index">
            <div class="item active">
                <div class="fill" style="background-image:url('<?php echo $baseUrl;?>/images/a.jpg');"></div>
               <!--  <div class="carousel-caption">
                    <h4>Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet 1</h4>
                </div> -->
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $baseUrl;?>/images/b.jpg');"></div>
                <!-- <div class="carousel-caption">
                    <h4>Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet 2</h4>
                </div> -->
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $baseUrl;?>/images/c.jpg');"></div>
               <!--  <div class="carousel-caption">
                    <h4>Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet Lorem ipsum dilor sit amet 3</h4>
                </div> -->
            </div>
        </div>
        <hr>
    </header>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>About Us</h2>
                <hr>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                 <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                 <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
            <div class="col-md-4">
                 <h2>Sidebar</h2>
                 <hr>
               <div class="row sidebar">
                   <div class="col-md-5">
                       <img src="<?php echo $baseUrl;?>/images/test.png" style="height:140px;">
                   </div>
                   <div class="col-md-7">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                   </div>
               </div>
               <div class="row sidebar">
                   <div class="col-md-5">
                       <img src="<?php echo $baseUrl;?>/images/test.png" style="height:140px;">
                   </div>
                   <div class="col-md-7">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                   </div>
               </div>
               <div class="row sidebar">
                   <div class="col-md-5">
                       <img src="<?php echo $baseUrl;?>/images/test.png" style="height:140px;">
                   </div>
                   <div class="col-md-7">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                   </div>
               </div>
               <div class="row sidebar">
                   <div class="col-md-5">
                       <img src="<?php echo $baseUrl;?>/images/test.png" style="height:140px;">
                   </div>
                   <div class="col-md-7">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                   </div>
               </div>
               <div class="row sidebar">
                   <div class="col-md-5">
                       <img src="<?php echo $baseUrl;?>/images/test.png" style="height:140px;">
                   </div>
                   <div class="col-md-7">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                   </div>
               </div>
            </div>
        </div>
        <hr>
        <footer>
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <p>HAK CIPTA ©2015 PT.CITRA PAMUNGKAS MANDIRI</p>
                    </center>
                </div>
                <!-- <div class="col-md-8 menu-footer">
                    <a href="#">MENU 1</a>
                    <a href="#">MENU 2</a>
                    <a href="#">MENU 3</a>
                    <a href="#">MENU 4</a>
                    <a href="#">MENU 5</a>
                </div> -->
            </div>
        </footer>
    </div>
    <!-- /.container -->
    
    <!-- jQuery -->
    <script src="<?php echo $baseUrl;?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $baseUrl;?>/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

    <!-- circle menu -->
    <script src="<?php echo $baseUrl;?>/js/socialCircle.js"></script> 
    <script type="text/javascript">
    $( ".socialCircle-center" ).socialCircle({
        rotate: 0,
        radius:200,
        circleSize: 2,
        speed:500
    });
    </script>
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-36251023-1']);
      _gaq.push(['_setDomainName', 'jqueryscript.net']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>

</body>

</html>
