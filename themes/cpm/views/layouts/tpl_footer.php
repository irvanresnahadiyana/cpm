<?php

      $baseUrl = Yii::app()->theme->baseUrl; 

      $cs = Yii::app()->getClientScript();

      Yii::app()->clientScript->registerCoreScript('jquery');

    ?>
<footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="<?php echo $baseUrl;?>/js/jquery.js"></script>
    <script src="<?php echo $baseUrl;?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $baseUrl;?>/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo $baseUrl;?>/js/jquery.isotope.min.js"></script>
    <script src="<?php echo $baseUrl;?>/js/main.js"></script>
    <script src="<?php echo $baseUrl;?>/js/wow.min.js"></script>
</body>
</html>